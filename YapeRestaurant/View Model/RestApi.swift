//
//  RestApi.swift
//  YapeRestaurant
//
//  Created by Donnadony Mollo on 29/08/22.
//

import Foundation
import Alamofire
import SwiftyJSON


// GENERIC
enum APIResponse<T> {
    case success(T)
    case failure(Error)
}



// Protocol
protocol RestAPI {
    
    func callService(withURL urlString:String,
                     verb: HTTPMethod,
                     headers:HTTPHeaders?,
                     body:Parameters?,
                     completion:@escaping(APIResponse<JSON>) -> Void)
    
    func callService(withURL urlString:String,
                     completion:@escaping(APIResponse<JSON>) -> Void)
}


// Extension for RestAPI

extension RestAPI {
    
    var authHeader:HTTPHeaders{
        let contentType = "application/json"
        return ["Content-Type": contentType]
    }
    
   func callService(withURL urlString:String,
                    verb: HTTPMethod,
                    headers:HTTPHeaders?,
                    body:Parameters?,
                    completion:@escaping(APIResponse<JSON>) -> Void){
    
    let afrequest = AF.request(urlString,
                                      method: verb,
                                      parameters: body,
                                      encoding: JSONEncoding.default,
                                      headers: headers)
    
    
    
    afrequest.validate().responseJSON { (response) in
        
        switch response.result {
            case .success(let value):
                let json = JSON(value)
                completion(.success(json))
            case .failure(let error):
                
                completion(.failure(error))
            }
        }
    }
    
    func callService(withURL urlString:String,
                     completion:@escaping(APIResponse<JSON>) -> Void){
        
        
        

     
        let afrequest = AF.request(urlString)
        
        
        
        afrequest.validate().responseJSON { (response) in
            
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                completion(.success(json))
            case .failure(let error):
                
                completion(.failure(error))
            }
        }
    }
    
    
}


