//
//  AppViewModel.swift
//  YapeRestaurant
//
//  Created by Donnadony Mollo on 29/08/22.
//

import Foundation
import SwiftUI
import SwiftyJSON
import Alamofire
import MapKit

class AppViewModel: ObservableObject, RestAPI {
    
    @Published var currentMenu: String = ""
    @Published var loading = false
    
    @Published var filters: [String] = []
    @Published var recipes: [RecipeModel] = [RecipeModel]()
    @Published var coordinateRegion: MKCoordinateRegion = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 12.124985, longitude: -77.017592), latitudinalMeters: 1000, longitudinalMeters: 1000)

    
    func getRecipes() {
        
        self.loading = true
        
        DispatchQueue.main.async {
            self.callService(withURL: Constants.API_URL.recipes) { (response) in
                switch response {
                case .failure(let error):
                    self.loading = false
                    print(error)
                case .success(let json):
                    self.recipes = self.getListFromRecipes(from: json["data"])
                    self.filters = self.getListString(from: json["filters"])
                    self.loading = false
                }
            }
        }
    }
    
    func getListFromRecipes(from json:JSON) -> [RecipeModel] {
        var ws: [RecipeModel] = []
        for (_,sJson):(String, JSON) in json {
            var w = RecipeModel()
            w.copy(sJson)
            ws.append(w)
        }
        return ws
    }
    
    
    func getListString(from json:JSON) -> [String] {
        var ws: [String] = []
        for (_,sJson):(String, JSON) in json {
            ws.append(sJson.stringValue)
        }
        return ws
    }
    
    
}
