//
//  Constants.swift
//  YapeRestaurant
//
//  Created by Donnadony Mollo on 29/08/22.
//

import Foundation


enum Constants {
    
    enum Network {
        static let url = "https://demo5655901.mockable.io/"
    }
    
    enum API_URL {
        static let recipes = "\(Constants.Network.url)recipes"
    }
    
    
}
