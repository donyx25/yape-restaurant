//
//  YapeRestaurantApp.swift
//  YapeRestaurant
//
//  Created by Donnadony Mollo on 29/08/22.
//

import SwiftUI

@main
struct YapeRestaurantApp: App {
    let persistenceController = PersistenceController.shared
    
    @ObservedObject var viewModel = AppViewModel()

    var body: some Scene {
        WindowGroup {
            //ContentView()
            HomeView()
                .environmentObject(viewModel)
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
