//
//  RecipeModel.swift
//  YapeRestaurant
//
//  Created by Donnadony Mollo on 29/08/22.
//

import Foundation
import SwiftyJSON

struct RecipeModel:Identifiable {
    
    var id: Int = 0
    var image: String = ""
    var nombre: String = ""
    var descripcion : String = ""
    var showMap : Bool = false
    var mark: Bool = false
    var timeAprox: String  = ""
    var maxCalories : String = ""
    var tipo : String = ""
    var favorites: Int = 0
    var isFavorite: Bool = false
    
    var latitude: Double = 0
    var longitude: Double = 0
    
    var ingredients : [String] = []
    
    
    func getIngredients() -> String {
        return ingredients.joined(separator: "$")
    }
    
    mutating func copy(_ json: JSON){
        id = json["id"].intValue
        image = json["image"].stringValue
        nombre = json["nombre"].stringValue
        descripcion = json["descripcion"].stringValue
        showMap = json["showMap"].boolValue
        mark = json["mark"].boolValue
        timeAprox = json["time_aprox"].stringValue
        tipo = json["tipo"].stringValue
        isFavorite = json["is_favorite"].boolValue
        maxCalories = json["max_calories"].stringValue
        favorites = json["favorites"].intValue
        longitude = json["longitude"].doubleValue
        latitude = json["latitude"].doubleValue
        
        ingredients = getListString(from: json["ingredients"])
    }
    
    
    
    func getListString(from json:JSON) -> [String] {
        var ws: [String] = []
        for (_,sJson):(String, JSON) in json {
            ws.append(sJson.stringValue)
        }
        return ws
    }

}
