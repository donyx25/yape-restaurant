//
//  RecipeCellView.swift
//  YapeRestaurant
//
//  Created by Donnadony Mollo on 29/08/22.
//

import SwiftUI

struct RecipeCellView: View {
    
    var recipe: RecipeModel
    
    var body: some View {
        
        NavigationLink {
            RecipeDetailView(recipe: recipe)
        } label: {
            HStack(alignment: .top){
                
                AsyncImage(url: URL(string: recipe.image)) { image in
                    image
                        .resizable()
                        .scaledToFill()
                        .frame(width: 100)
                        .cornerRadius(10)
                    
                } placeholder: {
                    VStack(alignment:.center){
                        Spacer(minLength: 0)
                        ProgressView()
                        Spacer(minLength: 0)
                    }
                    .frame(width: 100)
                    .cornerRadius(10)
                }
                
                Spacer(minLength: 0)
                VStack(alignment: .leading){
                    Text(recipe.nombre.uppercased())
                        .font(.headline)
                        .lineLimit(nil)
                        .multilineTextAlignment(.leading)
                    
                    /*
                     Text(recipe.descripcion)
                         .multilineTextAlignment(.leading)
                         .lineLimit(nil)
                         .font(.caption)
                     */
                    
                    HStack{
                        Text(recipe.timeAprox)
                        Spacer(minLength: 0)
                        Text(recipe.maxCalories)
                    }
                    .padding([.top,.bottom],5)
                    
                    
                    if recipe.favorites > 0 {
                        Button {
                            
                        } label: {
                            
                            HStack{
                                Image(systemName: recipe.isFavorite ? "heart.fill" : "heart")
                                     .resizable()
                                     .scaledToFit()
                                     .frame(width:20)
                                     .foregroundColor(Color.button)
                                Text("\(recipe.favorites)")
                            }
                           
                        }
                    }
                    

                    
                }
                .padding([.leading,.trailing])
            }
            
        }

        
        
    }
}
