//
//  RecipeDetailView.swift
//  YapeRestaurant
//
//  Created by Donnadony Mollo on 29/08/22.
//

import SwiftUI
import CoreLocation
import MapKit






struct RecipeDetailView: View {
        
    @State var recipe: RecipeModel
    
    @State var ingredients: [String] = [
        "4 papas medianas, hervidas y frías",
        "1 tarro de leche evaporada",
        "6 galletas tipo agua",
        "Aceite, cantidad necesaria",
        "50 g de queso de cabra",
        "1 ají en escabeche",
        "Sal, a gusto",
        "Hojas de lechuga para decorar",
        "Huevo duro",
        "Aceitunas negras"]
    @State var region = MKCoordinateRegion(
        center:  CLLocationCoordinate2D (
            latitude: -12.0451522, longitude: -77.0357003
        ), span: MKCoordinateSpan(
            latitudeDelta: 0.05, longitudeDelta: 0.50
        )
    )
    @State private var  places: [Place]?
  
    var body: some View {
        
        
        
        ScrollView(.vertical, showsIndicators: false) {
            VStack(alignment:.leading){
                
                AsyncImage(url: URL(string: recipe.image)) { image in
                    image
                        .resizable()
                        .scaledToFit()
                    
                } placeholder: {
                    Spacer(minLength: 0)
                    ProgressView()
                    Spacer(minLength: 0)
                }
                Spacer(minLength: 0)
                
                HStack{
                    Text(recipe.nombre)
                        .font(.title)
                    Spacer(minLength: 0)
                    
                    
                    if recipe.favorites > 0 {
                        Button {
                            if recipe.isFavorite {
                                self.recipe.favorites = recipe.favorites - 1
                            }
                            else {
                                self.recipe.favorites = recipe.favorites + 1
                            }
                            self.recipe.isFavorite.toggle()
                        } label: {
                            
                            HStack{
                                Image(systemName: recipe.isFavorite ? "heart.fill" : "heart")
                                     .resizable()
                                     .scaledToFit()
                                     .frame(width:20)
                                     .foregroundColor(Color.button)
                                Text("\(recipe.favorites)")
                                    .foregroundColor(.black)
                            }
                           
                        }
                    }
                    
                }
                .padding([.leading,.trailing,.top])
                    
                
                HStack{
                    Text(recipe.timeAprox)
                        .font(.body)
                    Spacer(minLength: 0)
                    Text(recipe.maxCalories)
                        .font(.body)
                }
                .padding([.leading,.trailing])
                
                HStack{
                    Text("Descripción")
                        .font(.title)
                        .multilineTextAlignment(.leading)
                        .lineLimit(nil)
                    
                    Spacer(minLength: 0)
                    
                }
                .padding([.leading,.trailing])
                
                HStack{
                    Text(recipe.descripcion)
                        .font(.body)
                    Spacer(minLength: 0)
                }
                .padding([.leading,.trailing])
                
                
                
                HStack{
                    Text("Ingredients")
                        .font(.title)
                    
                    Spacer(minLength: 0)
                    
                    if places != nil {
                        NavigationLink {
                            Map(coordinateRegion: $region, showsUserLocation: false,  annotationItems: places!){ place in
                                    MapPin(coordinate: place.coordinate)
                                    //MapMarker(coordinate: place.coordinate)
                            }
                            .edgesIgnoringSafeArea(.all)
                        } label: {
                            HStack{
                                Image(systemName: "location.fill")
                                    .resizable()
                                    .scaledToFit()
                                    .frame(width:20)
                                    .foregroundColor(Color.button)
                                
                            }
                        }
                    } else {
                        VStack{
                            Spacer()
                            ProgressView()
                            Spacer()
                        }
                    }
                    
                }
                .padding([.leading,.trailing])
                
                
                ForEach(0..<recipe.ingredients.count, id: \.self) { index in
                    
                    Text("* \(recipe.ingredients[index])")
                        .font(.body)
                        .padding([.leading,.trailing])
                        .padding(.top,5)
                }
            }
            
        }
        .onAppear{
            
            region.center = CLLocationCoordinate2D (
                latitude: recipe.latitude, longitude: recipe.longitude
            )
            region.span =  MKCoordinateSpan(
                latitudeDelta: 0.1, longitudeDelta: 0.1
            )
            
            self.places = [  Place(name: "Position 1", latitude: recipe.latitude, longitude: recipe.longitude) ]
        }
        .edgesIgnoringSafeArea(.top)
        
        
        
    }
}
