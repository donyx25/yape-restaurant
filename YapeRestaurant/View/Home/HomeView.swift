//
//  HomeView.swift
//  YapeRestaurant
//
//  Created by Donnadony Mollo on 29/08/22.
//

import SwiftUI

struct HomeView: View {
    
    @State var queryString = ""
    @State var listRecipes: [RecipeModel] = [
    
        RecipeModel(id: 1, image: "", nombre: "", descripcion: "", showMap: false, mark: false)
    ]
    
    @EnvironmentObject var viewModel: AppViewModel
    
    @State var isFilteringGroup = false
    @State var isFilteringSearch = ""
    @State var loading = true
    
    var body: some View {
        NavigationView{
            
            VStack{
                
                
                if viewModel.loading {
                    ProgressView()
                }
                
                else {
                    ScrollView (.horizontal, showsIndicators: false) {
                         HStack {
                             //contents
                             
                             ForEach(0..<viewModel.filters.count, id: \.self) { index in
                                 buttonFilter(desc: viewModel.filters[index])
                             }
                         }
                         .padding(5)
                         .padding([.leading,.trailing])
                    }
                   
                    
                    List{
                        ForEach(listRecipesSearch()) { recipe in
                            RecipeCellView(recipe: recipe)
                                .onAppear{
                                    print(recipe)
                                }
                        }
                    }
                    .refreshable {
                        self.viewModel.getRecipes()
                    }
                    .listStyle(.plain)
                }
                
            }
           
            .onChange(of: queryString, perform: { newValue in
                if queryString.count > 0 {
                    isFilteringGroup = false
                }
            })
            .onAppear{
                viewModel.getRecipes()
            }
            .navigationTitle(Text("Lista de recetas"))
            
            
        }
        .navigationViewStyle(.stack)
        .searchable(text: $queryString, placement: .navigationBarDrawer(displayMode: .always))

    }
    
    func buttonFilter(desc: String) -> some View {
        Button {
            withAnimation (Animation.easeInOut) {
                isFilteringGroup = true
                isFilteringSearch = desc
            }
            
        } label: {
            Text(desc)
                .foregroundColor(.white)
                .padding()
        }
        .frame(height: 40,alignment: .center)
        .background(Color.button)
        .cornerRadius(10)

    }
    
    
    func listRecipesSearch() -> [RecipeModel] {
        
        if isFilteringGroup {
            if isFilteringSearch == "Todos" {
                return viewModel.recipes
            }
            else{
                return viewModel.recipes.filter { $0.tipo.lowercased().contains(isFilteringSearch.lowercased()) }
            }
            
        }
        else {
            if queryString.isEmpty {
                return viewModel.recipes
            }
            else {
                return viewModel.recipes.filter { $0.nombre.lowercased().contains(queryString.lowercased()) ||  $0.getIngredients().contains(queryString.lowercased()) }
            }
        }
        
        
    }
    
}
