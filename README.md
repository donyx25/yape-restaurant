# Code Challenge Yape – Mobile Developer

# Aplicacion de Restaurante

# The Challenge

En este paso queremos saber cómo pones en práctica tus habilidades. Para ello, te pedimos que
desarrolles una pequeña aplicación que te permita visualizar recetas de cocina.

## _Navigation APP_
En la aplicación se encuentra disponibles tres pantallas: 
- [HomeView] - Se muestra la lista de recetas disponibles, ahí mismo se puede realizar la búsqueda por nombre o ingredientes.También existe la posibilidad que filtrarlos por categorías.
- [DetailView] - Se muestra el detalle de la receta seleccionada.
- [MapView] - Se muestra las coordenadas especificadas en el detalle de la receta.

## Architecture patterns
- [MVVM]
Se utilizó la arquitectura MVVM , para dividir en un número pequeño de responsabilidades que tiene la aplicación las cuales deben estar bien definidas.
De tal manera que el código sea más fácil de entender, modificar y mantener.

## Librerías externas

- [Alamofire] 
Se usa alamofire para el consumo de servicios utilizados en la aplicación, así mismo realizar toda la comunicación con el servicio.

- [SwiftyJson]
Se usa para la conversión de datos que son adquiridos al realizar la consulta de los servicios.


## Otros
- [SwiftUI]
Se utilizó Swiftui para el desarrollo del challenge, en el que se aplica el uso de observadores para manejar los datos reactivos que se presentan en la pantalla de usuario.

- [www.mockable.io]
Se utilizó mockable io para obtener los servicios que utilizan la aplicación para la consulta de todas las recetas disponibles.


## License

**Donnadony Mollo Quicaño**
